## Smuulin sviippitreenin säännöt

### Pelin kulku

Smuulin sviippitreeni koostuu viidestä lajista, joiden pisteet lasketaan yhteen. Jokaisessa lajissa kentälle lajitellaan kahdeksan kyykkää ja jokaisen kyykän poistamisesta saa pisteen, joten maksimipisteet on 5 x 8 = 40. Papeista tai heittämättä jääneistä kartuista ei jaeta lisäpisteitä. Smuulin sviippitreenissä pelataan kaksi erää, joiden yhteistulos muodostaa lopullisen tuloksen. 

Alla oleva kuva esittelee kenttään piirrettävät merkit sekä jokaisen lajin kasausohjeet. Yksi väri vastaa yhtä linjaa.

![Lajien asetelmat](smuuli.png)

Linjat heitetään järjestyksessä oikealta vasemmalle ja lajeissa 1.-4. jokaiseen linjaan on käytettävissä yksi heitto. 5. lajissa molempiin linjoihin on käytettävissä kaksi heittoa. Ainoastaan vuorossa olevasta linjasta lähtevät kyykät lasketaan poistetuiksi ja vääristä linjoista poistuneet tai liikkuneet kyykät palautetaan paikoilleen ennen seuraavaa heittoa. Jos kyykkä siirtyy heitettävästä linjasta johonkin toiseen linjaan, siirretään se sivuun siten, ettei se vaikuta muihin linjoihin. Kentälle jäävä karttu poistetaan neliöstä ennen seuraavaa heittoa, lukuun ottamatta 5. lajia, jossa linjaan heitetyn ensimmäisen kartun jäädessä neliöön sitä ei poisteta.
