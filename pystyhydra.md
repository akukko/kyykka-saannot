## Pystyhydran säännöt

### Pelin kulku

Pystyhydra aloitetaan samankaltaisesta asetelmasta kuin Kyykkäliiton sääntöjen mukainen henkilökohtainen peli. Torneja on siis pelineliön eturajalla tasaisin välein kymmenen kappaletta, niin että reunimmaisista torneista on kentän sivurajoihin 1,25 metriä matkaa. Pystyhydraa saa heittää yhteen suuntaan kerrallaan, ilman että oman heittoneliön eturajalla on "vastustajan" kyykkäriviä. 

Avausheittoa ei tarvitse suorittaa, eli pelin voi avata henkilökohtaisesta pelistä poiketen neliön sisältä tai edemmiltä heittorajoilta. Halutessaan voi silti myös heittää neliön takaa.

Peliä pelataan yksi maila kerrallaan. Neliöön jääneet mailat poistetaan heiton jälkeen. 

Jokaisen heiton jälkeen heitosta jääneet akat (kyykät jotka eivät ole etuviivalla osana tornia) palautetaan riviin niin, että jokaisesta kentällä olevasta yksittäisestä akasta syntyy uusi torni riviin. 

Mikäli heitolla ei synny yhtäkään akkaa kentälle (pelineliössä on ainoastaan etuviivalla sijaitsevia torneja), eikä yhtäkään kyykkää poistu neliöstä, tulee kentälle neljä uutta tornia.

### Tornien pystyttäminen

Torneja kasataan henkilökohtaisen pelin kona-alueen sisälle (1,25 m molemmista laidoista) niin kauan kuin henkkarialueen sisällä on vähemmän kuin kymmenen tornia. 

Mikäli torneja on kymmenen henkkarialueen sisällä, torneja kasataan olemassa olevan rivin jatkoksi (ei siis suoraan nurkkaan).

Tilanteessa, jossa heiton jälkeen henkkarialueen sisällä on kymmenen tornia, ja muualla kentällä on yksi tai useampi torni jotka eivät ole henkkarikonan jatkeena, saa pelaaja valita kumman rivin jatkoksi uudet tornit kasataan. Tässä tapauksessa riviksi lasketaan siis myös yksittäinen torni.

Pystytettävien tornien etäisyys toisistaan on sama kuin alkuasetelmassa. Mikäli tornien nostamisen jälkeen kolmen tai useamman tornin muodostama yhtenäinen rivi on huomattavan epätasaisin välein kasattu, voidaan kyseistä riviä korjata. Tämä korjaus on kuitenkin tehtävä niin, että rivin reunimmaiset tornit liikkuvat korkeintaan yhden tornien välisen etäisyyden verran alkuperäiseltä paikaltaan.

### Pelin päättyminen

Peli päättyy joko siihen, kun kentällä ei ole heiton jälkeen yhtäkään kyykkää, tai siihen kun kentälle tulisi heiton jälkeen enemmän kuin kaksikymmentä tornia.

Eriä pelataan kaksi ja tulos lasketaan tyhjennykseen käytettyjen mailojen määränä. Pienempi tulos on parempi. Mikäli jompi kumpi erä päättyy rivin täyttymiseen, tulos jää saamatta.
